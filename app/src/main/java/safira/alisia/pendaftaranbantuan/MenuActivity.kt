package safira.alisia.pendaftaranbantuan

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.bantuan_main.*
import kotlinx.android.synthetic.main.menu_main.*

class MenuActivity: AppCompatActivity() {
    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_main)

        if (FirebaseAuth.getInstance().uid.isNullOrEmpty()){
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
        btnLogout.setOnClickListener {
            fbAuth.signOut()
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
        btnDaftar.setOnClickListener {
            val i = Intent(this, BantuanActivity::class.java)
            startActivity(i)
        }
    }
}