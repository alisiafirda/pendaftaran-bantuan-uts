package safira.alisia.pendaftaranbantuan

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.bantuan_main.*

class BantuanActivity : AppCompatActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bantuan_main)

        if (FirebaseAuth.getInstance().uid.isNullOrEmpty()) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
//        tvData1 = findViewById(R.id.txKk)
//        if (intent.extras != null){
//            tvData1.setText(intent.getStringExtra("data1"))
//        }
//        tvData2 = findViewById(R.id.txKk)
//        if (intent.extras != null){
//            tvData2.setText(intent.getStringExtra("data2"))
//        }


        btnTambah.setOnClickListener(this)
        btnUp.setOnClickListener(this)
        uri = Uri.EMPTY
        alWarga = ArrayList()
        btnInsert.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }
    val COLLECTION = "pendaftaranbantuan"
    val F_NIK = "nik"
    val F_NAMA = "nama"
    val F_TTL = "ttl"
    val F_PEKERJAAN = "pekerjaan"
    val F_PENGHASILAN = "penghasilan"
    val F_ANGGOTA = "anggota"
    val F_DOKUMEN = "dokumen"
    var docNIK = ""
    lateinit var db : FirebaseFirestore
    lateinit var  alWarga : ArrayList<HashMap<String, Any>>
    lateinit var adapter : SimpleAdapter
    lateinit var uri : Uri
    private lateinit var filepath: Uri
    private lateinit var tvData1: TextView
    private lateinit var tvData2: TextView



    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("FireStore", e.message.toString())
            showData()
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alWarga.get(position)
        docNIK = hm.get(F_NIK).toString()
        ednik.setText(docNIK)
        ednama.setText(hm.get(F_NAMA).toString())
        edttl.setText(hm.get(F_TTL).toString())
        edpekerjaan.setText(hm.get(F_PEKERJAAN).toString())
        edpenghasilan.setText(hm.get(F_PENGHASILAN).toString())
        edanggota.setText(hm.get(F_ANGGOTA).toString())
//        txKk.setText(hm.get(F_DOKUMEN).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alWarga.clear()
            for(doc in result){
                val hm = HashMap<String, Any>()
                hm.set(F_NIK, doc.get(F_NIK).toString())
                hm.set(F_NAMA, doc.get(F_NAMA).toString())
                hm.set(F_TTL, doc.get(F_TTL).toString())
                hm.set(F_PEKERJAAN, doc.get(F_PEKERJAAN).toString())
                hm.set(F_PENGHASILAN, doc.get(F_PENGHASILAN).toString())
                hm.set(F_ANGGOTA, doc.get(F_ANGGOTA).toString())
//                hm.set(F_DOKUMEN, doc.get(F_DOKUMEN).toString())


                alWarga.add(hm)
            }
            adapter = SimpleAdapter(this, alWarga, R.layout.row_data,
                arrayOf(F_NIK, F_NAMA, F_TTL, F_PEKERJAAN, F_PENGHASILAN, F_ANGGOTA, F_DOKUMEN),
                intArrayOf(R.id.txNik, R.id.txNama, R.id.txTtl, R.id.txPekerjaan, R.id.txPenghasilan, R.id.txAnggota))
            lsData.adapter = adapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
//            R.id.btnTambah -> {
//                var intent = Intent(this, DokumenActivity::class.java)
//                startActivity(intent)
//            }
            R.id.btnInsert -> {
                val hm = HashMap<String, Any>()
                hm.set(F_NIK, docNIK)
                hm.set(F_NAMA, ednama.text.toString())
                hm.set(F_TTL, edttl.text.toString())
                hm.set(F_PEKERJAAN, edpekerjaan.text.toString())
                hm.set(F_PENGHASILAN, edpenghasilan.text.toString())
                hm.set(F_ANGGOTA, edanggota.text.toString())
//                hm.set(F_DOKUMEN, txKk.text.toString())
                db.collection(COLLECTION).document(ednik.text.toString()).set(hm) .addOnSuccessListener{
                    Toast.makeText(this, "Data Successfully added", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e->
                    Toast.makeText(this, "Data Unsuccessfully added : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnTambah -> {
                startFileChooser()
            }
            R.id.btnUp -> {
                uploadFile()
            }
        }
    }
    private fun uploadFile() {
        if(filepath!=null){
            var pd = ProgressDialog(this)
            pd.setTitle("Uploading")
            pd.show()

            var metaCursor = contentResolver.query(filepath, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME),null,null,null)!!
            metaCursor.moveToFirst()
            var fileName = metaCursor.getString(0)
            metaCursor.close()

            var imageRef = FirebaseStorage.getInstance().reference.child(fileName)
            imageRef.putFile(filepath)
                .addOnSuccessListener {p0 ->
                    pd.dismiss()
                    Toast.makeText(this,"File Uploaded", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {p0 ->
                    pd.dismiss()
                    Toast.makeText(applicationContext,p0.message, Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener {p0 ->
                    var progress = (100.0 * p0.bytesTransferred) / p0.totalByteCount
                    pd.setMessage("Uploading ${progress.toInt()}%")
                }
        }
    }

    private fun startFileChooser() {
        var i = Intent()
        i.setType("*/*")
        i.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(i,"Choose File"),111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==111 && resultCode == Activity.RESULT_OK && data != null) {
            filepath = data.data!!
            var bitmap = MediaStore.Images.Media.getBitmap(contentResolver,filepath)

        }
    }
}