package safira.alisia.pendaftaranbantuan

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.login_main.*
import java.lang.Exception

class LoginActivity : AppCompatActivity(), View.OnClickListener  {

    var bundle :Bundle? = null
    var topik = "tesnotifdanlogin"
    var type = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_main)

        btnLogin.setOnClickListener(this)
        textView2.setOnClickListener(this)
        //FirebaseMessaging.getInstance().subscribeToTopic(topik)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin -> {
                var nik = edUserName.text.toString()
                var password = edPassword.text.toString()

                if(nik.isEmpty() || password.isEmpty()){
                    Toast.makeText(this, "Username / Password can't be empty", Toast.LENGTH_SHORT).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating....")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(nik,password)
                        .addOnCompleteListener{
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, MenuActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            this.startActivity(intent)
                        }
                        .addOnFailureListener{
                            progressDialog.hide()
                            Toast.makeText(this, "Username / Password incorrect", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.textView2 -> {
                var intent = Intent(this, DaftarActivity::class.java)
                startActivity(intent)
            }
        }
    }
//    override fun onResume() {
//        super.onResume()
//
//        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
//            OnCompleteListener { task ->
//                if (!task.isSuccessful) return@OnCompleteListener
//                txToken.setText(task.result!!.token)
//            }
//        )
//        try {
//            bundle = getIntent().getExtras()!!
//        }catch (e: Exception){
//            Log.e("BUNDLE", "bundle is null")
//        }
//
//        if (bundle != null){
//            type = bundle!!.getInt("type")
//            when(type){
//                1 -> {
//                    txTitle.setText(bundle!!.getString("title"))
//                    txDesc.setText(bundle!!.getString("body"))
//                }
//            }
//        }
//    }
}