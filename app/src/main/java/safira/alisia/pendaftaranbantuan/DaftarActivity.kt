package safira.alisia.pendaftaranbantuan

import android.app.ProgressDialog
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.daftar_main.*
import com.google.firebase.auth.FirebaseAuth as FirebaseAuth

class DaftarActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daftar_main)

        btnRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var nik = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()

        if (nik.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Username / Password can't be empty", Toast.LENGTH_LONG).show()
        } else {
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering.....")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(nik, password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Successfully Register", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_SHORT).show()
                }
        }
    }
}